import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import mock from './mock.js'
import './index.css'

const key = () => Math.round(Math.random() * (100000000000 - 1) + 1);

const Item = ({ data, index }) => (
  <span className="dataItem" style={{background: index % 2 === 0 ? "#ececec" : "#fff"}}>{data}</span>
);

const List = ({ data }) => (
  <div className="dataList">
    {data.map((item, index) => <Item key={key()} data={item} index={index} />)}
  </div>
);

class App extends Component {
  state = { 
    data: mock,
    run: false, 
    time: {
      js: 0,
      wasm: 0
    }
  }

  javascriptSort = (data) => {
    this.setState({run: true})
    const t0 = performance.now();
    const jsSort = new Promise((resolve, reject) => {
      this.setState({ data: data.sort(), run: false });
      resolve(true);
    });
    jsSort.then(() => {
      const t1 = performance.now();
      this.setState({ time: { js: (t1 - t0)} });
    });
  }

  render() {
    const { run, time } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div>
          <div>
            <h2>Power of WASM</h2>
          </div>

          <div className="wrapper">
          <div className="container">
            <h3 className="title">1</h3>
            <button disable={run} onClick={() => this.javascriptSort(mock)}>Sort with JS</button>
            {time.js ? <h4>JS time is {time.js}</h4> : null}
            <List data={mock} />  
          </div>
          <div className="container">
            <h3 className="title">2</h3>
            {time.wasm ? <h4>JS time is {time.wasm}</h4> : null}
            <List data={mock} />
          </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
