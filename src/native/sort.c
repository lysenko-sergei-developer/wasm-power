#include "sort.h"
#include <stdio.h>
#include <stdlib.h>

void swap(char *xp, char *yp) {
    char temp = *xp;
    *xp = *yp;
    *yp = temp;
}

char **bubbleSort(char **arr, int size) {
  int i, j;
  char *tempWord1 = (char *) malloc(32);
  char *tempWord2 = (char *) malloc(32); 

  for (i = 0; i < size-1; i++) {
    for (j = 0; j < size-i-1; j++) {
      tempWord1 = arr[j];
      tempWord2 = arr[j+1];

      if (tempWord1[0] > tempWord2[0]) {
        swap(&arr[j], &arr[j+1]);
      }
    } 
  }      

  return arr;
}